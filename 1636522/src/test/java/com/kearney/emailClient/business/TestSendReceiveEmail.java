/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kearney.emailClient.business;

import org.junit.*;
import com.kearney.emailclient.business.SendEmail;
import com.kearney.emailclient.business.ReceiveEmail;
import com.kearney.emailclient.data.EmailBean;
import com.kearney.emailclient.data.AttachmentBean;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import jodd.mail.EmailAddress;
import static org.junit.Assert.*;

/**
 *
 * @author 1636522
 */
public class TestSendReceiveEmail {
    private SendEmail send;
    private ArrayList<EmailAddress> receivers;
    private ArrayList<EmailAddress> others;
    private ArrayList<AttachmentBean> files;
    private ReceiveEmail re;
    private EmailBean e;
    private EmailBean badE;
    private EmailBean eCC;
    private EmailBean eBCC;
    private EmailBean eTxt;
    private EmailBean eHTML;
    private EmailBean eAtt;
    private EmailBean eEmb;
    private String pwd;
    private String smtp;
    private String imap;
    private AttachmentBean a;
    private EmailAddress sender;
    private EmailAddress receiver;
    private EmailAddress other;
    
    @Ignore
    @Before
    public void init() throws IOException{
        pwd = "Felixxilef123";
        smtp = "smtp.gmail.com";
        imap = "imap.gmail.com";
        
        receiver = new EmailAddress("Joe Bob", "receive.1636522@gmail.com");
        other = new EmailAddress("Joe Bob", "other.1636522@gmail.com");
        send = new SendEmail(pwd, smtp);
        re = new ReceiveEmail(receiver.getEmail(),pwd, imap);
        receivers = new ArrayList<EmailAddress>();
        receivers.add(receiver);
        others = new ArrayList<EmailAddress>();
        others.add(other);
        files = new ArrayList<AttachmentBean>();
        a = new AttachmentBean();
        a.setContent(Files.readAllBytes(Paths.get("test.png")));
        a.setName("test.png");
        files.add(a);
        sender = new EmailAddress("Joe Bob", "send.1636522@gmail.com");
        
        e = new EmailBean();
        e.setSender(sender);
        e.setReceivers(receivers);
        e.setSubject("test");
        
        badE = new EmailBean();
        badE.setSender(new EmailAddress("blah", "blah"));
        badE.setReceivers(receivers);
        badE.setSubject("test");
        
        eCC = new EmailBean();
        eCC.setSender(sender);
        eCC.setReceivers(receivers);
        eCC.setSubject("test");
        eCC.setCc(others);
        
        eBCC = new EmailBean();
        eBCC.setSender(sender);
        eBCC.setReceivers(receivers);
        eBCC.setSubject("test");
        eBCC.setBcc(others);
        
        eTxt = new EmailBean();
        eTxt.setSender(sender);
        eTxt.setReceivers(receivers);
        eTxt.setSubject("test");
        eTxt.setTextMessage("test test");
        
        eHTML = new EmailBean();
        eHTML.setSender(sender);
        eHTML.setReceivers(receivers);
        eHTML.setSubject("test");
        eHTML.setHtmlMessage("<h1>test</h1>");
        
        eAtt = new EmailBean();
        eAtt.setSender(sender);
        eAtt.setReceivers(receivers);
        eAtt.setSubject("test");
        eAtt.setAttachments(files);
        
        
        
    }
    @Ignore
    @Test
    public void basicEmailTest() throws InterruptedException{
        send.send(e);
        Thread.sleep(5000);
        EmailBean[] emails = re.receive();
        assertEquals(e, emails[0]);
    }
    
    //@Test
    //public void invalidEmailTest(){
   //     assertFalse(send.send(badE));
    //}
    @Ignore
    @Test
    public void EmailCCTest() throws InterruptedException{
        send.send(eCC);
        Thread.sleep(5000);
        EmailBean[] emails = re.receive();
        assertEquals(eCC, emails[emails.length - 1]);
    }
    @Ignore    
    @Test
    public void EmailBCCTest() throws InterruptedException{
        send.send(eBCC);
        Thread.sleep(5000);
        EmailBean[] emails = re.receive();
        assertEquals(eBCC, emails[emails.length - 1]);
    }
    @Ignore  
    @Test
    public void EmailTextMessageTest() throws InterruptedException{
        send.send(eTxt);
        Thread.sleep(5000);
        EmailBean[] emails = re.receive();
        assertEquals(eTxt, emails[emails.length - 1]);
    }
    @Ignore    
    @Test
    public void EmailHTMLMessageTest() throws InterruptedException{
        send.send(eHTML);
        Thread.sleep(5000);
        EmailBean[] emails = re.receive();
        assertEquals(eHTML, emails[emails.length - 1]);
    }
    @Ignore    
    @Test
    public void EmailAttatchmentTest() throws InterruptedException{
        send.send(eAtt);
        Thread.sleep(5000);
        EmailBean[] emails = re.receive();
        assertEquals(eAtt, emails[emails.length - 1]);
    }
        
    
    
    
    
}
