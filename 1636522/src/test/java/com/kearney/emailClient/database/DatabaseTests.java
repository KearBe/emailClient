/*
 * Database Access Object tests
 */
package com.kearney.emailClient.database;

import org.junit.*;
import com.kearney.emailclient.database.EmailDAO;
import com.kearney.emailclient.database.FolderDAO;
import com.kearney.emailclient.data.*;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Date;
import java.sql.*;
import jodd.mail.EmailAddress;
import static org.junit.Assert.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
/**
 *
 * @author 1636522
 */
public class DatabaseTests {
    private EmailDAO edao = new EmailDAO("jdbc:mysql://localhost:3306/email?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
    "mailboy", "mailpwd");
    private FolderDAO fdao = new FolderDAO("jdbc:mysql://localhost:3306/email?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
    "mailboy", "mailpwd");
    private EmailBean email = new EmailBean();
    private ArrayList<AttachmentBean> files;
    private EmailAddress sender;
    private EmailAddress receiver;
    private EmailAddress other;
    private ArrayList<EmailAddress> receivers;
    private ArrayList<EmailAddress> others;
    private AttachmentBean a;
    
    @Before
    public void init() throws IOException{
        receiver = new EmailAddress("Joe Bob", "receive.1636522@gmail.com");
        other = new EmailAddress("Joe Bob", "other.1636522@gmail.com");
        sender = new EmailAddress("Joe Bob", "send.1636522@gmail.com");
        receivers = new ArrayList<EmailAddress>();
        receivers.add(receiver);
        others = new ArrayList<EmailAddress>();
        others.add(other);
        files = new ArrayList<AttachmentBean>();
        a = new AttachmentBean();
        a.setContent(Files.readAllBytes(Paths.get("test.png")));
        a.setName("test.png");
        a.setIsEmbedded("0");
        files.add(a);
        
        email.setSender(sender);
        email.setReceivers(receivers);
        email.setCc(others);
        email.setBcc(others);
        email.setSubject("subject");
        email.setTextMessage("test test");
        email.setHtmlMessage("<h1>test test</h1>");
        email.setAttachments(files);
        email.setSendTime(LocalDateTime.now());
        email.setReceiveTime(LocalDateTime.now());
        email.setFolder("inbox");
        email.setPriority(0);
    }
    @Ignore
    @Test
    public void emailCreateTest() throws InterruptedException, SQLException{
        edao.create(email);
        EmailBean newEmail = edao.findId(edao.findAll().size());
        edao.delete(edao.findAll().size());
        assertEquals(email, newEmail);
    }
    @Test
    public void folderCreateTest() throws InterruptedException, SQLException{
        fdao.create("newFolder");
        String newFolder = fdao.findFolder("newFolder");
        fdao.delete("newFolder");
        assertEquals("newFolder", newFolder);
    }
    @Ignore
    @Test
    public void emailDeleteTest() throws InterruptedException, SQLException{
        edao.create(email);
        int before = edao.findAll().size();
        edao.delete(edao.findAll().size());
        int after = edao.findAll().size();
        assertNotEquals(before, after);
    }
    
    @Test
    public void folderDeleteTest() throws InterruptedException, SQLException{
        fdao.create("newFolder");
        int before = fdao.findAll().size();
        fdao.delete("newFolder");
        int after = fdao.findAll().size();
        assertNotEquals(before, after);
    }
    
    @Test
    public void emailUpdateTest() throws InterruptedException, SQLException{
        edao.create(email);
        edao.update("sent", edao.findAll().size());
        EmailBean newEmail = edao.findId(edao.findAll().size());
        edao.delete(edao.findAll().size());
        assertNotEquals(email.getFolder(), newEmail.getFolder());
    }
    
    @Test
    public void folderUpdateTest() throws InterruptedException, SQLException{
        fdao.create("newFolder");
        fdao.update("newFolder", "newNewFolder");
        String updatedFolder = fdao.findFolder("newNewFolder");
        fdao.delete(updatedFolder);
        assertNotEquals("newFolder", updatedFolder);
    }
    
    @Test
    public void emailFindInFolderTest() throws InterruptedException, SQLException{
        edao.create(email);
        List<EmailBean> emails = edao.findInFolder(email.getFolder());
        edao.delete(edao.findAll().size());
        int check = 0;
        for(int i = 0; i < emails.size(); i++){
            if(email.equals(emails.get(i))){
                check = 1;
                assertTrue(true);
            }
        }
        if(check == 0){
            assertTrue(false);
        }
    }
    
    @Test
    public void emailFindByAddressTest() throws InterruptedException, SQLException{
        edao.create(email);
        List<EmailBean> emails = edao.findByAddress(email.getCc().get(0).getEmail());
        edao.delete(edao.findAll().size());
        int check = 0;
        for(int i = 0; i < emails.size(); i++){
            if(email.equals(emails.get(i))){
                check = 1;
                assertTrue(true);
            }
        }
        if(check == 0){
            assertTrue(false);
        }
    }
    
    @Before
    public void seedDatabase() throws FileNotFoundException, IOException {

        String s = new String();
        StringBuffer sb = new StringBuffer();
 
        FileReader fr = new FileReader(new File("DDL.sql"));
 
        BufferedReader br = new BufferedReader(fr);
 
        while((s = br.readLine()) != null){
            sb.append(s);
        }
        br.close();
 
        String[] inst = sb.toString().split(";");
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/email?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
    "root", "dawson");Statement st = connection.createStatement();) {
            for(int i = 0; i<inst.length; i++){
                if(!inst[i].trim().equals("")){
                    st.executeUpdate(inst[i]);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
}
//TODO: tests for other reads, folder tests