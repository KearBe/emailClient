/*
 * DAO for Folders
 */
package com.kearney.emailclient.database;

import java.sql.*;
import java.util.*;

/**
 *
 * @author 1636522
 */
public class FolderDAO implements IFolderDAO{
    
    private static String URL;
    private static String USER;
    private static String PWD;
    
    public FolderDAO(String url, String user, String pwd){
        this.URL = url;
        this.USER = user;
        this.PWD = pwd;
    }
    
    /**
     * Takes a string and makes a folder of that name in the database
     * 
     * @param String folderName
     * @return number of inserted rows
     * @throws SQLException 
     */
    @Override
    public int create(String folderName) throws SQLException{
        boolean isInTable = false;
        List<String> folders = findAll();
        for(int i = 0; i < folders.size(); i++){
            if(folderName.equals(folders.get(i))){
                isInTable = true;
            }
        }
        int result = 0;
        if(!isInTable){
            String createQuery = "INSERT INTO FOLDER (FOLDERNAME) VALUES (?)";
        
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                        PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, folderName);
            
                result = ps.executeUpdate();
            }
        }
        return result;
    }
    
    /**
     * Finds all of the folders in the database and returns them as a list of 
     * Strings
     * 
     * @return list of folder names
     * @throws SQLException 
     */
    @Override
    public List<String> findAll() throws SQLException{
        List<String> folders = new ArrayList<>();
        String createQuery = "SELECT * FROM FOLDER";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);
                ResultSet rs = ps.executeQuery()){
            while(rs.next()){
                folders.add(rs.getString("FOLDERNAME"));
            }
        }
        return folders;
    }
    
    /**
     * Finds all of the folders in the database and returns them as a list of 
     * Strings
     * 
     * @return list of folder names
     * @throws SQLException 
     */
    @Override
    public String findFolder(String name) throws SQLException{
        String folder = "";
        String createQuery = "SELECT * FROM FOLDER WHERE FOLDERNAME = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                folder = rs.getString("FOLDERNAME");
            }
        }
        return folder;
    }
    
    /**
     * Updates a folder's name
     * 
     * @param oldName
     * @param newName
     * @return
     * @throws SQLException 
     */
    @Override
    public int update(String oldName, String newName) throws SQLException{
        int result = 0;
        if((!oldName.equalsIgnoreCase("inbox")) || (!oldName.equalsIgnoreCase("sent"))){
            String createQuery = "UPDATE FOLDER SET FOLDERNAME = ? WHERE FOLDERNAME = ?";
        
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, newName);
                ps.setString(2, oldName);
            
                result = ps.executeUpdate();
                updateEmails(oldName, newName);
            }
        }
        return result;
    }
    /**
     * private method that updates the folder name for all the contained emails 
     * when a folder's name is changed
     * 
     * @param oldName
     * @param newName
     * @throws SQLException 
     */
    private void updateEmails(String oldName, String newName) throws SQLException{
        String createQuery = "UPDATE EMAILBEAN SET FOLDERNAME = ? WHERE FOLDERNAME = ?";
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, newName);
            ps.setString(2, oldName);
            
            ps.executeUpdate();
        }
    }
    
    /**
     * deletes a given folder from the database
     * 
     * @param name
     * @return
     * @throws SQLException 
     */
    @Override
    public int delete(String name) throws SQLException{
        int result = 0;
        if((!name.equalsIgnoreCase("inbox")) || (!name.equalsIgnoreCase("sent"))){
            String createQuery = "DELETE FROM FOLDER WHERE FOLDERNAME = ?";
        
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, name);
            
                result = ps.executeUpdate();
            }
        }
        return result;
    }
    /**
     * Private method that deletes all emails contained in a deleted folder
     * 
     * @param name
     * @throws SQLException 
     */
    private void deleteEmails(String name) throws SQLException{
        String createQuery = "DELETE FROM EMAILBEAN WHERE FOLDERNAME = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, name);
            ps.executeUpdate();
        }
    }
    
}
