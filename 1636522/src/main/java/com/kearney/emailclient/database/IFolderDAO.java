/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kearney.emailclient.database;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author 1636522
 */
public interface IFolderDAO {
    
    //Create
    public int create(String folderName) throws SQLException;
    
    //Read
    public List<String> findAll() throws SQLException;
    
    public String findFolder(String folder) throws SQLException;
    
    //Update
    public int update(String oldName, String newName) throws SQLException;
    
    //Delete
    public int delete(String name) throws SQLException;
    
}
