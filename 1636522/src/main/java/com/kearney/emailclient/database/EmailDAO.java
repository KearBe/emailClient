/*
 * DAO for Emails
 */
package com.kearney.emailclient.database;

import java.sql.*;
import java.util.*;
import com.kearney.emailclient.data.EmailBean;
import com.kearney.emailclient.data.AttachmentBean;
import java.io.ByteArrayInputStream;
import java.time.ZoneId;
import jodd.mail.EmailAddress;

/**
 *
 * @author 1636522
 */
public class EmailDAO implements IEmailDAO{
    
    private static String URL;
    private static String USER;
    private static String PWD;
    
    public EmailDAO(String url, String user, String pwd){
        this.URL = url;
        this.USER = user;
        this.PWD = pwd;
    }
    
    /**
     * 
     * Takes an EmailBean and inserts it into the database
     * 
     * @param EmailBean email
     * @return number of inserted rows
     * @throws SQLException 
     */
    @Override
    public int create(EmailBean email) throws SQLException{
        boolean isInTable = false;
        List<EmailBean> emails = findAll();
        for(int i = 0; i < emails.size(); i++){
            if(email.equals(emails.get(i))){
                isInTable = true;
            }
        }
        int result = 0;
        if(!isInTable){
            String createQuery = "INSERT INTO EMAILBEAN VALUES (?,?,?,?,?,?,?,?,?,?)";
            int id = (int) (findAll().size()+ (Math.random() * 100));//TODO: fix this
            /**List<Integer> ids = new ArrayList<Integer>();
            for(EmailBean e : emails){
                ids.add(new Integer(email.getId()));
                System.out.println(email.getId());
            }
            int id = Collections.max(ids);
            * */
            
        
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setInt(1, id);
                ps.setString(2, email.getFolder());
                ps.setString(3, email.getSender().getEmail());
                ps.setString(4, email.getSender().getPersonalName());
                ps.setString(5, email.getSubject());
                ps.setString(6, email.getTextMessage());
                ps.setString(7, email.getHtmlMessage());
                ps.setDate(8, new java.sql.Date(java.util.Date.from(email.getSendTime().toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime()));
                ps.setDate(9, new java.sql.Date(java.util.Date.from(email.getReceiveTime().toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant()).getTime()));
                ps.setInt(10, email.getPriority());
            
                result = ps.executeUpdate();
                addReceivers(email, id);
                if(email.getAttachments() != null){
                    addAttachment(email, id);
                }
                if(email.getCc() != null){
                    addCC(email, id);
                }
                if(email.getBcc() != null){
                    addBCC(email, id);
                }
            }
        }
        return result;
    }
    /**
     * Private method called by the create method that adds the email's 
     * attachments to the attachment table if any
     * 
     * @param EmailBean email
     * @param int emailID
     * @throws SQLException 
     */
    private void addAttachment(EmailBean email, int emailID) throws SQLException{
        String createQuery = "INSERT INTO ATTACHMENT (EMAILID, NAME, CONTENT, TYPE) VALUES(?,?,?,?)";
        for(int i = 0; i < email.getAttachments().size(); i++){
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setInt(1, emailID);
                ps.setString(2, email.getAttachments().get(i).getName());
                ps.setBlob(3, new ByteArrayInputStream(email.getAttachments().get(i).getContent()));
                ps.setString(4, email.getAttachments().get(i).getIsEmbedded());
                
                ps.executeUpdate();
            }
        }
    }
    /**
     * Private method called by the create method that adds the email's 
     * CCs to the bridging table if any
     * 
     * @param EmailBean email
     * @param int emailID
     * @throws SQLException 
     */
    private void addCC(EmailBean email, int emailID) throws SQLException{
        String createQuery = "INSERT INTO TYPE (TYPE, EMAILID, ADDRESS) VALUES (?,?,?)";
        for(int i = 0; i < email.getCc().size(); i++){
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, "CC");
                ps.setInt(2, emailID);
                ps.setString(3, email.getCc().get(i).getEmail());
                addAddress(email.getCc().get(i).getEmail(), email.getCc().get(i).getPersonalName());
                
                ps.executeUpdate();
            }
        }
    }
    /**
     * Private method called by the create method that adds the email's 
     * BCCs to the bridging table if any
     * 
     * @param EmailBean email
     * @param int emailID
     * @throws SQLException 
     */
    private void addBCC(EmailBean email, int emailID) throws SQLException{
        String createQuery = "INSERT INTO TYPE (TYPE, EMAILID, ADDRESS) VALUES (?,?,?)";
        for(int i = 0; i < email.getBcc().size(); i++){
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, "BCC");
                ps.setInt(2, emailID);
                ps.setString(3, email.getBcc().get(i).getEmail());
                addAddress(email.getBcc().get(i).getEmail(), email.getBcc().get(i).getPersonalName());
                
                ps.executeUpdate();
            }
        }
    }
    /**
     * Private method called by the create method that adds the email's 
     * receivers to the bridging table
     * 
     * @param EmailBean email
     * @param int emailID
     * @throws SQLException 
     */
    private void addReceivers(EmailBean email, int emailID) throws SQLException{
        String createQuery = "INSERT INTO TYPE (TYPE, EMAILID, ADDRESS) VALUES (?,?,?)";
        for(int i = 0; i < email.getReceivers().size(); i++){
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, "TO");
                ps.setInt(2, emailID);
                ps.setString(3, email.getReceivers().get(i).getEmail());
                addAddress(email.getReceivers().get(i).getEmail(), email.getReceivers().get(i).getPersonalName());
                
                ps.executeUpdate();
            }
        }
    }
    /**
     * Private method called by the previous three method that adds the emails 
     * to the emailAddress table
     * 
     * @param EmailBean email
     * @param int emailID
     * @throws SQLException 
     */
    private void addAddress(String address, String name) throws SQLException{
        boolean isInTable = false;
        ArrayList<String> addresses = findAddresses();
        for(int i = 0; i < addresses.size(); i++){
            if(address.equals(addresses.get(i))){
                isInTable = true;
            }
        }
        
        if(!isInTable){
            String createQuery = "INSERT INTO EMAILADDRESS (ADDRESS, NAME) VALUES (?,?)";
            try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                    PreparedStatement ps = connection.prepareStatement(createQuery);){
                ps.setString(1, address);
                ps.setString(2, name);
            
                ps.executeUpdate();
            }
        }
    }
    /**
     * private method to find all addresses in the address table
     * 
     * @return list of address strings
     * @throws SQLException 
     */
    private ArrayList<String> findAddresses() throws SQLException{
        ArrayList<String> addresses = new ArrayList<String>();
        String createQuery = "SELECT * FROM EMAILADDRESS";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);
                ResultSet rs = ps.executeQuery()){
            while(rs.next()){
                addresses.add(rs.getString("ADDRESS"));
            }
        }
        return addresses;
    }
    /**
     * finds all of the emails in the database and returns them as a list of 
     * emailBeans
     * 
     * @return arraylist of emailBeans
     * @throws SQLException 
     */
    @Override
    public List<EmailBean> findAll() throws SQLException{
        List<EmailBean> emails = new ArrayList<EmailBean>();
        String createQuery = "SELECT * FROM EMAILBEAN";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);
                ResultSet rs = ps.executeQuery()){
            while (rs.next()) {
                emails.add(createEmailBean(rs));
            }
        }
        return emails;
    }
    
    @Override
    public EmailBean findId(int id) throws SQLException{
        
        EmailBean email = new EmailBean();
        String createQuery = "SELECT * FROM EMAILBEAN WHERE EMAILID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
            PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setInt(1, id);
            {
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    email = createEmailBean(rs);
                }
            }
        }
        
        return email;    
        
    }
    
    /**
     * finds all of the emails in the database that are in a given folder and 
     * returns them as a list of emailBeans
     * 
     * @param String folder name
     * @return arraylist of emailBeans
     * @throws SQLException 
     */
    @Override
    public List<EmailBean> findInFolder(String folder) throws SQLException{
        List<EmailBean> emails = new ArrayList<>();
        String createQuery = "SELECT * FROM EMAILBEAN WHERE FOLDERNAME = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, folder);
            
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                emails.add(createEmailBean(rs));
            }
        }
        return emails;
    }
    
    /**
     * finds all of the emails in the database that are associated with a given  
     * email address and returns them as a list of emailBeans
     * 
     * @param String email address
     * @return arraylist of emailBeans
     * @throws SQLException 
     */
    @Override
    public List<EmailBean> findByAddress(String address) throws SQLException{
        List<EmailBean> emails = new ArrayList<>();
        String createQuery = "SELECT * FROM EMAILBEAN JOIN TYPE USING (EMAILID) WHERE ADDRESS = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, address);
            
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                emails.add(createEmailBean(rs));
            }
        }
        return emails;
    }
    
    /**
     * Private method that creates an object of type EmailBean from the current
     * record in the ResultSet
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private EmailBean createEmailBean(ResultSet rs) throws SQLException{
        EmailBean email = new EmailBean();
        email.setId(rs.getInt("EMAILID"));
        email.setSender(new EmailAddress(rs.getString("FROMNAME"),rs.getString("FROM")));
        email.setSubject(rs.getString("SUBJECT"));
        email.setTextMessage(rs.getString("TEXTMSG"));
        email.setHtmlMessage(rs.getString("HTMLMESG"));
        email.setSendTime(new java.util.Date(rs.getDate("SENDDATE").getTime()));
        email.setReceiveTime(new java.util.Date(rs.getDate("RECEIVEDATE").getTime()));
        email.setReceivers(findAddresses(rs.getInt("EMAILID"), "TO"));
        email.setCc(findAddresses(rs.getInt("EMAILID"), "CC"));
        email.setBcc(findAddresses(rs.getInt("EMAILID"), "BCC"));
        email.setAttachments(findAttachments(rs.getInt("EMAILID")));
        return email;
    }
    /**
     * Private method that finds all the email addresses of a given type 
     * (CC, BCC, TO) that are associated with a given email
     * 
     * @param id
     * @param type
     * @return list of EmailAddress
     * @throws SQLException 
     */
    private ArrayList<EmailAddress> findAddresses(int id, String type) throws SQLException{
        ArrayList<EmailAddress> addresses = new ArrayList<EmailAddress>();
        String createQuery = "SELECT * FROM EMAILADDRESS JOIN TYPE USING (ADDRESS) JOIN EMAILBEAN USING (EMAILID) WHERE (TYPE = ?) AND (EMAILBEAN.EMAILID = ?)";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, type);
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                EmailAddress ea = new EmailAddress(rs.getString("NAME"), rs.getString("ADDRESS"));
                addresses.add(ea);
            }
        }
        
        return addresses;
    }
    /**
     * Private method that finds all the attachments that are associated with a 
     * given email
     * 
     * @param id
     * @return list of AttachmentBean
     * @throws SQLException 
     */
    private ArrayList<AttachmentBean> findAttachments(int id) throws SQLException{
        ArrayList<AttachmentBean> attachments = new ArrayList<AttachmentBean>();
        String createQuery = "SELECT * FROM ATTACHMENT WHERE EMAILID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                AttachmentBean att = new AttachmentBean();
                att.setContent(rs.getBytes("CONTENT"));
                att.setName(rs.getString("NAME"));
                att.setIsEmbedded(rs.getString("TYPE"));
                attachments.add(att);
            }
        }
        
        return attachments;
    }
    
    /**
     * Updates the Folder that a given Email is in
     * 
     * @param folderID
     * @param emailID
     * @return number of records updated
     * @throws SQLException 
     */
    @Override
    public int update(String folderID, int emailID) throws SQLException{
        int result = 0;
        String createQuery = "UPDATE EMAILBEAN SET FOLDERNAME = ? WHERE EMAILID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setString(1, folderID);
            ps.setInt(2, emailID);
            
            result = ps.executeUpdate();
        }
        return result;
    }
    
    /**
     * deletes a given email from the database
     * 
     * @param id
     * @return number of deleted records
     * @throws SQLException 
     */
    @Override
    public int delete(int id) throws SQLException{
        int result = 0;
        String createQuery = "DELETE FROM EMAILBEAN WHERE EMAILID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setInt(1, id);
            deleteAttachment(id);
            deleteType(id);
            result = ps.executeUpdate();
        }
        
        return result;
    }
    /**
     * a private method to delete any attachments associated with an email that 
     * has just been deleted
     * 
     * @param emailID
     * @throws SQLException 
     */
    private void deleteAttachment(int emailID) throws SQLException{
        String createQuery = "DELETE FROM ATTACHMENT WHERE EMAILID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setInt(1, emailID);
            ps.executeUpdate();
        }
    }
    /**
     * a private method to delete any entries in the bridging table associated 
     * with an email that has just been deleted
     * 
     * @param emailID
     * @throws SQLException 
     */
    private void deleteType(int emailID) throws SQLException{
        String createQuery = "DELETE FROM TYPE WHERE EMAILID = ?";
        
        try (Connection connection = DriverManager.getConnection(URL, USER, PWD);
                PreparedStatement ps = connection.prepareStatement(createQuery);){
            ps.setInt(1, emailID);
            ps.executeUpdate();
        }
    }
}
