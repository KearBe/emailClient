/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kearney.emailclient.database;

import com.kearney.emailclient.data.EmailBean;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author 1636522
 */
public interface IEmailDAO {
    
    //Create
    public int create(EmailBean email) throws SQLException;
    
    //Read
    public List<EmailBean> findAll() throws SQLException;
    
    public EmailBean findId(int id) throws SQLException;
    
    public List<EmailBean> findInFolder(String folder) throws SQLException;
    
    public List<EmailBean> findByAddress(String address) throws SQLException;
    
    //Update
    public int update(String folderID, int emailID) throws SQLException;
    
    //Delete
    public int delete(int ID) throws SQLException;
}
