/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kearney.emailclient.data;

import java.io.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.*;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
/**
 *
 * @author 1636522
 */
public class EmailBean implements Serializable{
    private EmailAddress sender;
    private ArrayList<EmailAddress> receivers = new ArrayList<EmailAddress>();
    private ArrayList<EmailAddress> cc = new ArrayList<EmailAddress>();
    private ArrayList<EmailAddress> bcc = new ArrayList<EmailAddress>();
    private String subject;
    private String textMessage;
    private String htmlMessage;
    private ArrayList<AttachmentBean> attachments = new ArrayList<AttachmentBean>();
    private boolean isNew;
    private boolean reply;
    private boolean forward;
    private LocalDateTime sendTime;
    private LocalDateTime receiveTime;
    private String folder;
    private int priority;
    private int id;
    
    public StringProperty idProperty(){
        return new SimpleStringProperty(String.valueOf(id));
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public StringProperty priorityProperty(){
        return new SimpleStringProperty(String.valueOf(priority));
    }
    public StringProperty sendTimeProperty(){
        return new SimpleStringProperty(sendTime.toString());
    }
    public StringProperty receiveTimeProperty(){
        return new SimpleStringProperty(receiveTime.toString());
    }
    public StringProperty senderProperty(){
        return new SimpleStringProperty(sender.getEmail());
    }
    public StringProperty receiversProperty(){
        ArrayList<String> list = new ArrayList<String>();
        for(EmailAddress receiver : receivers){
            list.add(receiver.getEmail());
        }
        return new SimpleStringProperty(list.toString());
    }
    public StringProperty ccProperty(){
        ArrayList<String> list = new ArrayList<String>();
        for(EmailAddress cc : cc){
            list.add(cc.getEmail());
        }
        return new SimpleStringProperty(list.toString());
    }
    public StringProperty subjectProperty(){
        return new SimpleStringProperty(subject);
    }
    public StringProperty textMsgProperty(){
        return new SimpleStringProperty(textMessage);
    }
    public StringProperty htmlMsgProperty(){
        return new SimpleStringProperty(htmlMessage);
    }
    
    public EmailBean(){
        textMessage = "";
        subject = "";
        htmlMessage = "";
    }
    public String getTextMessage() {
        return textMessage;
    }
    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailBean other = (EmailBean) obj;
        if (!Objects.equals(this.textMessage, other.textMessage)) {
            return false;
        }
        if (!Objects.equals(this.sender.getEmail(), other.sender.getEmail())) {
            return false;
        }
        if (!Objects.equals(this.subject, other.subject)) {
            return false;
        }
        if(this.receivers.size() != other.receivers.size()){
            return false;
        }
        for(int i = 0; i < this.receivers.size(); i++){
            if(!Objects.equals(this.receivers.get(i).getEmail(), other.receivers.get(i).getEmail())){
                return false;
            }
        }
        for(int i = 0; i < this.cc.size(); i++){
            if(!Objects.equals(this.cc.get(i).getEmail(), other.cc.get(i).getEmail())){
                return false;
            }
        }
        if (!Objects.equals(this.attachments, other.attachments)) {
            return false;
        }
        return true;
    }
   

    public LocalDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }
    
    public void setSendTime(Date in) {
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        this.sendTime = ldt;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }
    
    public void setReceiveTime(Date in) {
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        this.receiveTime = ldt;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.sender);
        hash = 17 * hash + Objects.hashCode(this.receivers);
        hash = 17 * hash + Objects.hashCode(this.cc);
        hash = 17 * hash + Objects.hashCode(this.textMessage);
        hash = 17 * hash + Objects.hashCode(this.attachments);
        return hash;
    }

    public EmailAddress getSender() {
        return sender;
    }

    public void setSender(EmailAddress sender) {
        this.sender = sender;
    }

    public ArrayList<EmailAddress> getReceivers() {
        return receivers;
    }

    public void setReceivers(ArrayList<EmailAddress> receivers) {
        this.receivers = receivers;
    }

    public ArrayList<EmailAddress> getCc() {
        return cc;
    }

    public void setCc(ArrayList<EmailAddress> cc) {
        this.cc = cc;
    }

    public ArrayList<EmailAddress> getBcc() {
        return bcc;
    }

    public void setBcc(ArrayList<EmailAddress> bcc) {
        this.bcc = bcc;
    }
    
   

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHtmlMessage() {
        return htmlMessage;
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage = htmlMessage;
    }

    public ArrayList<AttachmentBean> getAttachments() {
        return attachments;
    }

    public void setAttachments(ArrayList<AttachmentBean> attachments) {
        this.attachments = attachments;
    }

    

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean getReply() {
        return reply;
    }

    public void setReply(boolean reply) {
        this.reply = reply;
    }

    public boolean getForward() {
        return forward;
    }

    public void setForward(boolean forward) {
        this.forward = forward;
    }

    
    
}
