package com.kearney.emailclient.view;

import com.kearney.emailclient.business.ReceiveEmail;
import com.kearney.emailclient.data.AttachmentBean;
import com.kearney.emailclient.data.EmailBean;
import com.kearney.emailclient.database.EmailDAO;
import com.kearney.emailclient.database.FolderDAO;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class BaseLayoutController {

    private EmailBean selectedEmail = null;
    
    @FXML
    private ListView<String> folders;
    
    @FXML
    private TextField folderName;
    
    @FXML
    private WebView view;
    
    @FXML
    private TableView<EmailBean> table;
    
    @FXML
    private TableColumn<EmailBean, String> toColumn;
    @FXML
    private TableColumn<EmailBean, String> fromColumn;
    @FXML
    private TableColumn<EmailBean, String> ccColumn;
    @FXML
    private TableColumn<EmailBean, String> subjectColumn;
    @FXML
    private TableColumn<EmailBean, String> sentdateColumn;
    @FXML
    private TableColumn<EmailBean, String> receiveddateColumn;
    @FXML
    private TableColumn<EmailBean, String> priorityColumn;
    
    ObservableList<String> list;
    
    private FolderDAO fdao = new FolderDAO("jdbc:mysql://localhost:3306/email?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
    "mailboy", "mailpwd");
    
    private EmailDAO edao = new EmailDAO("jdbc:mysql://localhost:3306/email?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
    "mailboy", "mailpwd");
    
    /**
     * initializes the list of folders and also binds each table column with
     * a field of en email bean
     * 
     * @author: Benjamin Kearney
     * @throws SQLException 
     */
    @FXML
    private void initialize() throws SQLException {
        list = FXCollections.observableArrayList(fdao.findAll());
        folders.setItems(list);
        fromColumn.setCellValueFactory(cellData -> cellData.getValue()
                .senderProperty());
        toColumn.setCellValueFactory(cellData -> cellData.getValue()
                .receiversProperty());
        ccColumn.setCellValueFactory(cellData -> cellData.getValue()
                .ccProperty());
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue()
                .subjectProperty());
        sentdateColumn.setCellValueFactory(cellData -> cellData.getValue()
                .sendTimeProperty());
        receiveddateColumn.setCellValueFactory(cellData -> cellData.getValue()
                .receiveTimeProperty());
        priorityColumn.setCellValueFactory(cellData -> cellData.getValue()
                .priorityProperty());
    }
    
    /**
     * updates the table based on which folder is selected
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    protected void handleFolderSelectAction(MouseEvent event) throws IOException, SQLException {
        String folder = this.folders.getSelectionModel().getSelectedItem();
        List<EmailBean> emails = edao.findInFolder(folder);
        table.setItems(FXCollections.observableArrayList(emails));
    }
    
    /**
     * Displays the selected Email in the webview
     * 
     * @author: Benjamin Kearney
     * @param event
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    protected void handleTableSelectAction(MouseEvent event) throws IOException, SQLException {
        EmailBean email = this.table.getSelectionModel().getSelectedItem();
        selectedEmail = email;
        String html = null;
        if(email.getSender().getPersonalName() != null){
            html = "<h1>"+email.getSender().getPersonalName()
                +"</h1>"+"<h2>"+email.getSubject()+":</h2><hr>"+email.getHtmlMessage();
            view.getEngine().loadContent(html);
        }else{
            html = "<h1>"+email.getSender().getEmail()
                +"</h1>"+"<h2>"+email.getSubject()+":</h2><hr>"+email.getHtmlMessage();
            view.getEngine().loadContent(html);
        }
        if(email.getAttachments() != null && email.getAttachments().size() > 0){
            html += "<br>Attachments: ";
            for(AttachmentBean ab : email.getAttachments()){
                html += ab.getName();
            }
            view.getEngine().loadContent(html);
        }
    }
    
    /**
     * adds the folder to the database and to the listview
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    protected void handleAddButtonAction(ActionEvent event) throws IOException, SQLException {
        list.add(folderName.getText());
        fdao.create(folderName.getText());
        folders.setItems(list);
    }
    
    /**
     * handles the close action in the file menu in the menubar
     * 
     * @author Benjamin Kearney
     * @param event 
     */
    @FXML
    protected void close(ActionEvent event){
        Platform.exit();
    }
    
    /**
     * deletes the selected email
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws SQLException 
     */
    @FXML
    protected void deleteEmail(ActionEvent event) throws SQLException{
        edao.delete(selectedEmail.getId());
        String folder = this.folders.getSelectionModel().getSelectedItem();
        List<EmailBean> emails = edao.findInFolder(folder);
        table.setItems(FXCollections.observableArrayList(emails));
    }
    
    /**
     * deletes the selected folder
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws SQLException 
     */
    @FXML
    protected void deleteFolder(ActionEvent event) throws SQLException{
        String folder = this.folders.getSelectionModel().getSelectedItem();
        fdao.delete(folder);
        List<EmailBean> emails = edao.findInFolder("inbox");
        table.setItems(FXCollections.observableArrayList(emails));
        list = FXCollections.observableArrayList(fdao.findAll());
        folders.setItems(list);
    }
    
    /**
     * displays the readme in the webview
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws SQLException 
     */
    @FXML
    protected void about(ActionEvent event) throws SQLException, IOException{
        URL url = getClass().getResource("/html/readme.html");
        view.getEngine().load(url.toExternalForm());
    }
    
    /**
     * opens the compose window
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void handleComposeButtonAction(ActionEvent event) throws IOException {
        Stage primaryStage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Compose.fxml"));
        AnchorPane rootLayout = (AnchorPane) loader.load();
        
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /**
     * opens the forward window
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void handleForwardButtonAction(ActionEvent event) throws IOException {
        if(selectedEmail != null){
            Stage primaryStage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Forward.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
        
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            
            ForwardController fwd = loader.<ForwardController>getController();
            fwd.setEmail(selectedEmail);
            
            primaryStage.show();
        }
    }
    
    /**
     * opens the reply window
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void handleReplyButtonAction(ActionEvent event) throws IOException {
        if(selectedEmail != null){
            Stage primaryStage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Reply.fxml"));
            AnchorPane rootLayout = (AnchorPane) loader.load();
        
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            
            ReplyController re = loader.<ReplyController>getController();
            re.setEmail(selectedEmail);
            
            primaryStage.show();
        }
    }
    
    /**
     * receives all unread mail and displays it
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    protected void handleReceiveButtonAction(ActionEvent event) throws IOException, SQLException {
        Platform.runLater(() ->{
                ReceiveEmail re = new ReceiveEmail("receive.1636522@gmail.com","Felixxilef123", "imap.gmail.com");
                EmailBean[] emails = re.receive();
                for(EmailBean email : emails){
                    email.setFolder("inbox");
                    try {
                        edao.create(email);
                    } catch (SQLException ex) {
                        Logger.getLogger(BaseLayoutController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                List<EmailBean> emailList = null;
                    try {
                        emailList = edao.findInFolder("inbox");
                    } catch (SQLException ex) {
                        Logger.getLogger(BaseLayoutController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                table.setItems(FXCollections.observableArrayList(emailList));
            }
        );
    }
    
    /**
     * puts selected email into the dragboard and puts relevant content onto
     * the clipboard
     * 
     * @author
     * @param event 
     */
    @FXML
    private void dragDetected(MouseEvent event) {

        Dragboard db = table.startDragAndDrop(TransferMode.ANY);

        /* put a string on dragboard */
        ClipboardContent content = new ClipboardContent();
        content.putString(String.valueOf(table.getSelectionModel().getSelectedItem().getId()));

        db.setContent(content);

        event.consume();
    }
    
    /**
     * sets the folders to be able to accept the item in the dragboard
     * 
     * @author Benjamin Kearney
     * @param event 
     */
    @FXML
    private void folderDragOver(DragEvent event) {

        
        if (event.getGestureSource() != folders && event.getDragboard().hasString()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }
    
    /**
     * puts the dragged email into the specified folder
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws SQLException 
     */
    @FXML
    private void folderDragDropped(DragEvent event) throws SQLException {
        Dragboard db = event.getDragboard();
        String[] temp = event.getPickResult().getIntersectedNode().toString().split("'");
        String folderName = temp[1];
        boolean success = false;
        if (db.hasString()) {
            edao.update(folderName, Integer.valueOf(db.getString()));
            List<EmailBean> emails = edao.findInFolder(folderName);
            table.setItems(FXCollections.observableArrayList(emails));
            success = true;
        }
        event.setDropCompleted(success);

        event.consume();
    }
    
}
