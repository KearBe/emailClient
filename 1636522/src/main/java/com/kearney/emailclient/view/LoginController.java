package com.kearney.emailclient.view;

import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController {

    
    @FXML
    private void initialize() {
    }
    
    /**
     * goes to the next window
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void handleSubmitButtonAction(ActionEvent event) throws IOException {
        Stage primaryStage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BaseLayout.fxml"));
        AnchorPane rootLayout = (AnchorPane) loader.load();
        
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
