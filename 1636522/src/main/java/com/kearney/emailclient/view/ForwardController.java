package com.kearney.emailclient.view;

import com.kearney.emailclient.business.SendEmail;
import com.kearney.emailclient.data.AttachmentBean;
import com.kearney.emailclient.data.EmailBean;
import com.kearney.emailclient.database.EmailDAO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import jodd.mail.EmailAddress;

public class ForwardController {

    ArrayList<AttachmentBean> attList = new ArrayList<AttachmentBean>();
    
    private EmailBean email;
    @FXML
    private TextField toField;
    @FXML
    private TextField ccField;
    @FXML
    private TextField bccField;
    @FXML
    private TextField subjectField;
    @FXML
    private HTMLEditor editor;
    
    private EmailDAO edao = new EmailDAO("jdbc:mysql://localhost:3306/email?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", 
    "mailboy", "mailpwd");
    
    @FXML
    private void initialize() {
    }
    
    /**
     * method used to pass an email between controllers
     * 
     * @author Benjamin Kearney
     * @param e 
     */
    public void setEmail(EmailBean e){
        email = e;
        subjectField.setText(email.getSubject());
        if(email.getSender().getPersonalName() != null){
            editor.setHtmlText("<br><br><hr><h2>"+email.getSender().getPersonalName()+" Said:</h2>"+email.getHtmlMessage());
        }else{
            editor.setHtmlText("<br><br><hr><h2>"+email.getSender().getEmail()+" Said:</h2>"+email.getHtmlMessage());
        }
    }
    
    /**
     * sends the email and stores it in the database
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException
     * @throws SQLException 
     */
    @FXML
    protected void handleSendButtonAction(ActionEvent event) throws IOException, SQLException {
        
        //EmailBean email = new EmailBean();
        email.setSender(new EmailAddress("Joe Bob","receive.1636522@gmail.com"));
        String[] receivers = toField.getText().split(",");
        ArrayList<EmailAddress> receiverList = new ArrayList<EmailAddress>();
        for(String address : receivers){
            receiverList.add(new EmailAddress("",address));
        }
        email.setReceivers(receiverList);
        if((!"".equals(ccField.getText())) && ccField.getText()!=null){
            String[] CCs = ccField.getText().split(",");
            ArrayList<EmailAddress> ccList = new ArrayList<EmailAddress>();
            for(String address : CCs){
                ccList.add(new EmailAddress("",address));
            }
            email.setCc(ccList);
        }
        if((!"".equals(bccField.getText())) && bccField.getText()!=null){
            String[] BCCs = bccField.getText().split(",");
            ArrayList<EmailAddress> bccList = new ArrayList<EmailAddress>();
            for(String address : BCCs){
                bccList.add(new EmailAddress("",address));
            }
            email.setBcc(bccList);
        }
        email.setSubject("FWD:"+subjectField.getText());
        if((!"".equals(editor.getHtmlText())) && editor.getHtmlText()!=null){
            
            email.setHtmlMessage(editor.getHtmlText());
            email.setTextMessage(editor.getHtmlText().replaceAll("\\<.*?>","") );
        }
        email.setFolder("sent");
        email.setSendTime(new Date());
        email.setReceiveTime(new Date());
        email.setAttachments(attList);
        
        SendEmail se = new SendEmail("Felixxilef123","smtp.gmail.com");
        edao.create(email);
        se.send(email);
        
        Stage primaryStage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BaseLayout.fxml"));
        AnchorPane rootLayout = (AnchorPane) loader.load();
        
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
     /**
     * goes back to the previous window
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void handleBackButtonAction(ActionEvent event) throws IOException {
        Stage primaryStage = (Stage)(((Node)event.getSource()).getScene().getWindow());
            
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/BaseLayout.fxml"));
        AnchorPane rootLayout = (AnchorPane) loader.load();
        
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /**
     * adds an attachment to the email
     * 
     * @author Benjamin Kearney
     * @param event
     * @throws IOException 
     */
    @FXML
    protected void attachmentButton(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        AttachmentBean ab = new AttachmentBean();
        ab.setContent(Files.readAllBytes(file.toPath()));
        ab.setName(file.getName());
        attList.add(ab);
        editor.setHtmlText(editor.getHtmlText() + "<br>" + file.getName());
    }
}
