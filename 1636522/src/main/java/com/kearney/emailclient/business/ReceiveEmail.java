/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kearney.emailclient.business;

import com.kearney.emailclient.data.EmailBean;
import com.kearney.emailclient.data.AttachmentBean;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.activation.DataSource;
import javax.mail.Flags;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailFilter;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
/**
 *
 * @author 1636522
 */
public class ReceiveEmail {
    
    private String reEmail;
    private String pwd;
    private String imapServerName;

    public ReceiveEmail(String reEmail, String pwd, String imapServerName) {
        this.reEmail = reEmail;
        this.pwd = pwd;
        this.imapServerName = imapServerName;
    }
    
    public EmailBean[] receive(){
        EmailBean[] beans = null;
        if (checkEmail(reEmail)){
            ImapServer imapServer = MailServer.create()
                    .host(imapServerName)
                    .ssl(true)
                    .auth(reEmail, pwd)
                    .debugMode(false)
                    .buildImapMailServer();
            try (ReceiveMailSession session = imapServer.createSession()) {
                session.open();
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                if(emails != null){
                    beans = new EmailBean[emails.length];
                    for(int i = 0; i < emails.length; i++){
                        beans[i] = new EmailBean();
                    }
                    for(int i = 0; i < emails.length; i++){
                        beans[i].setSender(emails[i].from());
                        beans[i].setReceivers(new ArrayList<EmailAddress>(Arrays.asList(emails[i].to())));
                        beans[i].setSubject(emails[i].subject());
                        if(emails[i].cc() != null){
                            beans[i].setCc(new ArrayList<EmailAddress>(Arrays.asList(emails[i].cc())));
                        }
                        beans[i].setPriority(emails[i].priority());
                        beans[i].setSendTime(emails[i].sentDate());
                        beans[i].setReceiveTime(LocalDateTime.now());
                        
                        List<EmailMessage> messages = emails[i].messages();
                        if(messages != null){
                            for (EmailMessage msg : messages) {
                                if(msg.getMimeType().equals("TEXT/PLAIN")){
                                    beans[i].setTextMessage(msg.getContent());
                                }else{
                                    beans[i].setHtmlMessage(msg.getContent());
                                }
                            }
                        }
                        List<EmailAttachment<? extends DataSource>> attachments = emails[i].attachments();
                        if (attachments != null) {
                            ArrayList<AttachmentBean> as = new ArrayList<AttachmentBean>();
                            ArrayList<AttachmentBean> es = new ArrayList<AttachmentBean>();
                            for (EmailAttachment attachment : attachments) {
                                AttachmentBean a = new AttachmentBean();
                                a.setName(attachment.getName());
                                a.setContent(attachment.toByteArray());
                                as.add(a);
                                
                                //attachment.writeToFile(new File("C:\\Temp", attachment.getName()));
                            }
                            beans[i].setAttachments(as);
                        }
                    }
                }
            }
        }
        return beans;
    }
    
    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
     /**
     * Use the RFC2822AddressParser to validate that the email strings could be a
     * valid address
     *
     * @param addresses
     * @return true is OK, false if not
     */
    private boolean checkEmail(ArrayList<String> addresses) {
        for(int i = 0; i < addresses.size(); i++){
            if(RFC2822AddressParser.STRICT.parseToEmailAddress(addresses.get(i)) == null){
                return false;
            }
        }
        return true;
    }
}
