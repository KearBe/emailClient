/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kearney.emailclient.business;

import com.kearney.emailclient.data.EmailBean;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
/**
 *
 * @author 1636522
 */
public class SendEmail {
    
    private String pwd;
    private String smtpServerName;

    public SendEmail(String pwd, String smtpServerName) {
        this.pwd = pwd;
        this.smtpServerName = smtpServerName;
    }
    
    public boolean send(EmailBean email){
        if(email != null && email.getSender() != null && email.getReceivers() != null &&
                checkEmail(email.getSender().getEmail()) && checkEmail(email.getReceivers())){
                SmtpServer smtpServer = MailServer.create()
                    .ssl(true)
                    .host(smtpServerName)
                    .auth(email.getSender().getEmail(), pwd)
                    .debugMode(false)
                    .buildSmtpMailServer();
            
                Email e = Email.create().from(email.getSender())
                        .to(email.getReceivers().toArray(new EmailAddress[0]))
                        .subject(email.getSubject());
                if(email.getCc() != null && checkEmail(email.getCc())){
                    e.cc(email.getCc().toArray(new EmailAddress[0]));
                }
                if(email.getBcc() != null){
                    e.bcc(email.getBcc().toArray(new EmailAddress[0]));
                }
                if(email.getTextMessage() != null){
                    e.textMessage(email.getTextMessage());
                }
                if(email.getHtmlMessage() != null){
                    e.htmlMessage(email.getHtmlMessage());
                }
                if(email.getAttachments() != null){
                    for(int i = 0; i < email.getAttachments().size(); i++){
                        e.attachment(EmailAttachment.with().content(email.getAttachments().get(i).getContent()).name(email.getAttachments().get(i).getName()));
                    }
                }
                
                try (SendMailSession session = smtpServer.createSession()) {
                email.setSendTime(LocalDateTime.now());
                e.sentDate(new Date());
                session.open();
                session.sendMail(e);
                return true;
            }
        }
        return false;
    }
    
    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
     /**
     * Use the RFC2822AddressParser to validate that the email strings could be a
     * valid address
     *
     * @param addresses
     * @return true is OK, false if not
     */
    private boolean checkEmail(ArrayList<EmailAddress> addresses) {
        for(int i = 0; i < addresses.size(); i++){
            if(RFC2822AddressParser.STRICT.parseToEmailAddress(addresses.get(i).getEmail()) == null){
                return false;
            }
        }
        return true;
    }
}
