DROP TABLE IF EXISTS ATTACHMENT;
DROP TABLE IF EXISTS TYPE;
DROP TABLE IF EXISTS EMAILADDRESS;
DROP TABLE IF EXISTS EMAILBEAN;
DROP TABLE IF EXISTS FOLDER;

CREATE TABLE `emailaddress` (
  `address` VARCHAR(255) PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL
);

CREATE TABLE `folder` (
  `foldername` VARCHAR(255) PRIMARY KEY
);

CREATE TABLE `emailbean` (
  `emailid` INTEGER  PRIMARY KEY,
  `foldername` VARCHAR(255) NOT NULL,
  `from` VARCHAR(255) NOT NULL,
  `fromname` VARCHAR(255) NOT NULL,
  `subject` VARCHAR(400) NOT NULL,
  `textmsg` VARCHAR(5000) NOT NULL,
  `htmlmesg` VARCHAR(5000) NOT NULL,
  `senddate` DATETIME NOT NULL,
  `receivedate` DATETIME NOT NULL,
  `priority` INTEGER NOT NULL
);

CREATE INDEX `idx_emailbean__foldername` ON `emailbean` (`foldername`);

ALTER TABLE `emailbean` ADD CONSTRAINT `fk_emailbean__foldername` FOREIGN KEY (`foldername`) REFERENCES `folder` (`foldername`);

CREATE TABLE `attachment` (
  `attachmentid` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `emailid` INTEGER NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `content` LONGBLOB NOT NULL,
  `type` VARCHAR(255)
);

CREATE INDEX `idx_attachment__emailid` ON `attachment` (`emailid`);

ALTER TABLE `attachment` ADD CONSTRAINT `fk_attachment__emailid` FOREIGN KEY (`emailid`) REFERENCES `emailbean` (`emailid`);

CREATE TABLE `type` (
  `typeid` INTEGER  PRIMARY KEY AUTO_INCREMENT,
  `type` VARCHAR(255) NOT NULL,
  `emailid` INTEGER NOT NULL,
  `address` VARCHAR(255) NOT NULL
);

CREATE INDEX `idx_type__address` ON `type` (`address`);

CREATE INDEX `idx_type__emailid` ON `type` (`emailid`);

ALTER TABLE `type` ADD CONSTRAINT `fk_type__address` FOREIGN KEY (`address`) REFERENCES `emailaddress` (`address`);

ALTER TABLE `type` ADD CONSTRAINT `fk_type__emailid` FOREIGN KEY (`emailid`) REFERENCES `emailbean` (`emailid`);


insert into folder values ('inbox');
insert into folder values ('sent');

insert into emailbean values (1, 'inbox', 'ldominey0@phoca.cz', 'Lavinia Dominey', 'eu', 'magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet', 'quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis', '2018-08-29 09:21:02', '2017-12-27 21:18:31', 0);
insert into emailbean values (2, 'inbox', 'mbrickwood1@cisco.com', 'Mara Brickwood', 'gravida nisi at nibh in hac habitasse platea', 'sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam', 'diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu', '2017-12-22 20:23:38', '2018-08-09 16:12:10', 0);
insert into emailbean values (3, 'inbox', 'gvant2@auda.org.au', 'Georgetta Vant', 'nulla ut erat id mauris vulputate elementum nullam varius', 'vivamus tortor duis mattis egestas metus aenean fermentum donec ut', 'dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan felis', '2017-10-12 20:01:52', '2018-05-06 12:40:07', 0);
insert into emailbean values (4, 'inbox', 'ade3@cornell.edu', 'Abbey De Witt', 'ultrices phasellus id sapien in sapien iaculis', 'hac habitasse platea dictumst aliquam augue quam sollicitudin vitae consectetuer eget rutrum at lorem integer tincidunt ante', 'lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in', '2018-09-19 16:55:59', '2018-09-19 21:21:02', 0);
insert into emailbean values (5, 'inbox', 'lesgate4@360.cn', 'Leandra Esgate', 'imperdiet et commodo vulputate justo in', 'mattis nibh ligula nec sem duis aliquam convallis nunc proin at', 'at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in', '2017-12-18 05:19:14', '2018-05-07 11:26:01', 0);
insert into emailbean values (6, 'inbox', 'mmanginot5@qq.com', 'Myranda Manginot', 'congue risus semper porta', 'consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus', 'placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque', '2018-05-12 16:26:53', '2018-05-15 14:53:45', 0);
insert into emailbean values (7, 'inbox', 'esam6@feedburner.com', 'Emiline Sam', 'vulputate vitae nisl aenean lectus pellentesque eget', 'lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit', 'nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus', '2018-05-13 14:37:55', '2017-12-12 04:33:13', 0);
insert into emailbean values (8, 'inbox', 'bgoschalk7@rediff.com', 'Beverlie Goschalk', 'at lorem integer tincidunt ante vel ipsum praesent blandit', 'molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus', 'at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum', '2017-12-10 21:19:44', '2018-05-12 17:52:18', 0);
insert into emailbean values (9, 'inbox', 'nklossmann8@arizona.edu', 'Northrup Klossmann', 'ante vestibulum', 'integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis', 'phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae', '2018-06-13 23:27:09', '2018-01-11 11:50:37', 0);
insert into emailbean values (10, 'sent', 'eburehill9@amazon.co.jp', 'Eilis Burehill', 'eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor', 'odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla', 'interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis vel dapibus at diam nam tristique tortor', '2018-05-24 22:30:13', '2018-09-19 13:34:09', 0);
insert into emailbean values (11, 'sent', 'kdamsella@devhub.com', 'Korella Damsell', 'convallis tortor risus dapibus augue', 'neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia', 'leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa', '2018-07-24 19:38:50', '2018-04-02 14:50:40', 0);
insert into emailbean values (12, 'sent', 'lnearsb@oracle.com', 'Lemmie Nears', 'nisl nunc nisl duis bibendum felis sed interdum venenatis turpis', 'curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla', 'ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere', '2018-08-14 20:33:34', '2018-06-13 16:05:16', 0);
insert into emailbean values (13, 'sent', 'llaffertyc@ft.com', 'Libbi Lafferty', 'ultrices posuere cubilia curae duis', 'suspendisse ornare consequat lectus in est risus auctor sed tristique', 'imperdiet nullam orci pede venenatis non sodales sed tincidunt eu', '2018-06-01 15:32:50', '2017-11-03 20:31:06', 0);
insert into emailbean values (14, 'sent', 'jthorogoodd@deliciousdays.com', 'Justina Thorogood', 'suscipit nulla elit ac nulla sed vel', 'felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing', 'curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at', '2017-10-28 17:18:50', '2018-06-27 11:54:02', 0);
insert into emailbean values (15, 'sent', 'cmquhane@arizona.edu', 'Cordy M''Quhan', 'dolor sit amet', 'sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque', 'penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum', '2018-08-22 23:05:31', '2018-01-11 07:21:09', 0);
insert into emailbean values (16, 'sent', 'jigglesdenf@gov.uk', 'Jessalyn Igglesden', 'magna vestibulum aliquet ultrices erat tortor sollicitudin', 'elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus', 'massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id', '2018-07-02 14:22:01', '2017-11-02 01:07:49', 0);
insert into emailbean values (17, 'sent', 'nvawtong@nature.com', 'Nannette Vawton', 'accumsan felis ut at', 'euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam convallis nunc proin at turpis', 'ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam', '2018-03-17 14:36:11', '2018-04-21 16:37:55', 0);
insert into emailbean values (18, 'sent', 'shyderh@trellian.com', 'Sherman Hyder', 'potenti in eleifend quam a odio in hac habitasse platea', 'scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus', 'consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien in sapien iaculis congue', '2018-02-09 07:11:20', '2018-05-27 01:05:40', 0);
insert into emailbean values (19, 'sent', 'arowleri@technorati.com', 'Amity Rowler', 'aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed', 'id ligula suspendisse ornare consequat lectus in est risus auctor sed', 'erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi', '2017-11-03 03:19:00', '2017-11-04 11:50:43', 0);
insert into emailbean values (20, 'sent', 'amcgerraghtyj@vk.com', 'Arman McGerraghty', 'nulla sed vel enim sit amet nunc viverra dapibus', 'sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis', 'sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae', '2018-08-21 09:10:56', '2018-09-23 07:57:43', 0);

insert into attachment values (1, 1, 'smiledog.jpg', 'liuHI8979f8hfb987FGYygefHu', '1');
insert into attachment values (2, 10, 'coolpic.png', 'ikHKEfhig897EFfbkdhg987HIUFhiughEIUf89', '0');
insert into attachment values (3, 10, 'important_doc.doc', 'oieug8u9eHEGh090934jOIgjioJGR909GRO', '0');

insert into emailaddress values ('apretti0@nature.com', 'Anni Pretti');
insert into emailaddress values ('himmings1@slate.com', 'Hunfredo Immings');
insert into emailaddress values ('mvedmore2@hostgator.com', 'Milissent Vedmore');
insert into emailaddress values ('pphythien3@newsvine.com', 'Pren Phythien');
insert into emailaddress values ('msoutar4@wikispaces.com', 'Mufi Soutar');
insert into emailaddress values ('kclatworthy5@goo.gl', 'Kora Clatworthy');
insert into emailaddress values ('nfitchew6@delicious.com', 'Nicholle Fitchew');
insert into emailaddress values ('istorms7@ucsd.edu', 'Isaiah Storms');
insert into emailaddress values ('rhaggis8@hugedomains.com', 'Riley Haggis');
insert into emailaddress values ('dturfus9@psu.edu', 'Donaugh Turfus');
insert into emailaddress values ('bmessengera@theguardian.com', 'Bobinette Messenger');
insert into emailaddress values ('gbatchelderb@oracle.com', 'Gweneth Batchelder');
insert into emailaddress values ('rnobesc@mlb.com', 'Riobard Nobes');
insert into emailaddress values ('msaigerd@woothemes.com', 'Martynne Saiger');
insert into emailaddress values ('agrese@slashdot.org', 'Aleta Gres');
insert into emailaddress values ('balcidef@creativecommons.org', 'Baron Alcide');
insert into emailaddress values ('bmatteuccig@mail.ru', 'Beltran Matteucci');
insert into emailaddress values ('rharkessh@hexun.com', 'Rose Harkess');
insert into emailaddress values ('agierhardi@google.com.hk', 'Alameda Gierhard');
insert into emailaddress values ('rpicotj@bluehost.com', 'Rolph Picot');
insert into emailaddress values ('bdadda0@answers.com', 'Barth D''Adda');
insert into emailaddress values ('aroughey1@utexas.edu', 'Annette Roughey');
insert into emailaddress values ('hclemmen2@seattletimes.com', 'Hughie Clemmen');
insert into emailaddress values ('kcovell3@pagesperso-orange.fr', 'Kandy Covell');
insert into emailaddress values ('kvockings4@omniture.com', 'Klaus Vockings');
insert into emailaddress values ('dlambie5@newsvine.com', 'Doralyn Lambie');

insert into type values (1, 'TO', 1, 'apretti0@nature.com');
insert into type values (2, 'TO', 2, 'himmings1@slate.com');
insert into type values (3, 'TO', 3, 'mvedmore2@hostgator.com');
insert into type values (4, 'TO', 4, 'pphythien3@newsvine.com');
insert into type values (5, 'TO', 5, 'msoutar4@wikispaces.com');
insert into type values (6, 'TO', 6, 'kclatworthy5@goo.gl');
insert into type values (7, 'TO', 7, 'nfitchew6@delicious.com');
insert into type values (8, 'TO', 8, 'istorms7@ucsd.edu');
insert into type values (9, 'TO', 9, 'rhaggis8@hugedomains.com');
insert into type values (10, 'TO', 10, 'dturfus9@psu.edu');
insert into type values (11, 'TO', 11, 'bmessengera@theguardian.com');
insert into type values (12, 'TO', 12, 'gbatchelderb@oracle.com');
insert into type values (13, 'TO', 13, 'rnobesc@mlb.com');
insert into type values (14, 'TO', 14, 'msaigerd@woothemes.com');
insert into type values (15, 'TO', 15, 'agrese@slashdot.org');
insert into type values (16, 'TO', 16, 'balcidef@creativecommons.org');
insert into type values (17, 'TO', 17, 'bmatteuccig@mail.ru');
insert into type values (18, 'TO', 18, 'rharkessh@hexun.com');
insert into type values (19, 'TO', 19, 'agierhardi@google.com.hk');
insert into type values (20, 'TO', 20, 'rpicotj@bluehost.com');
insert into type values (21, 'CC', 1, 'bdadda0@answers.com');
insert into type values (22, 'CC', 1, 'aroughey1@utexas.edu');
insert into type values (23, 'CC', 1, 'hclemmen2@seattletimes.com');
insert into type values (24, 'BCC', 10, 'bdadda0@answers.com');
insert into type values (25, 'BCC', 10, 'aroughey1@utexas.edu');
insert into type values (26, 'BCC', 10, 'hclemmen2@seattletimes.com');